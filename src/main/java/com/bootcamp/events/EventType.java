package com.bootcamp.events;

/** enum for event type. */
public enum EventType {
  CREATED,
  UPDATED,
  DELETED
}
