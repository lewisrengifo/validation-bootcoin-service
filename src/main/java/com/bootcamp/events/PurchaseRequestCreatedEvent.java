package com.bootcamp.events;

import com.bootcamp.infraestructure.entity.PurchaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/** created event for purchase request. */
@Data
@EqualsAndHashCode
public class PurchaseRequestCreatedEvent extends Event<PurchaseRequest> {}
