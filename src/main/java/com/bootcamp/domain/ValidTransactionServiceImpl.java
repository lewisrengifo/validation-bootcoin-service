package com.bootcamp.domain;

import com.bootcamp.infraestructure.document.ValidTransactions;
import com.bootcamp.infraestructure.repository.ValidTransactionsRepository;
import com.bootcamp.infraestructure.service.ValidTransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

/** implementation for valid transaction service. */
@Service
@Slf4j
public class ValidTransactionServiceImpl implements ValidTransactionService {

  @Autowired ValidTransactionsRepository validTransactionsRepository;

  @Override
  public Mono<ValidTransactions> save(ValidTransactions validTransactions) {
    return validTransactionsRepository.save(validTransactions);
  }
}
