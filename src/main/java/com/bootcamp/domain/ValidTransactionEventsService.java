package com.bootcamp.domain;

import com.bootcamp.events.Event;
import com.bootcamp.events.EventType;
import com.bootcamp.events.PurchaseRequestCreatedEvent;
import com.bootcamp.infraestructure.entity.PurchaseRequest;
import java.util.Date;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/** valid transaction event service. */
@Component
@Slf4j
public class ValidTransactionEventsService {

  @Autowired private KafkaTemplate<String, Event<?>> producer;

  @Value("${topic.purchaseRequestValidation.name:purchaseRequestValidation}")
  private String topicPurchaseValidator;

  public void publish(PurchaseRequest purchaseRequest) {
    PurchaseRequestCreatedEvent createdEvent = new PurchaseRequestCreatedEvent();
    createdEvent.setId(UUID.randomUUID().toString());
    createdEvent.setDate(new Date());
    createdEvent.setType(EventType.CREATED);
    createdEvent.setData(purchaseRequest);

    this.producer.send(topicPurchaseValidator, createdEvent);
  }
}
