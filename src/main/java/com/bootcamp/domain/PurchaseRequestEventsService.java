package com.bootcamp.domain;

import com.bootcamp.events.Event;
import com.bootcamp.events.EventType;
import com.bootcamp.events.PurchaseRequestCreatedEvent;
import com.bootcamp.infraestructure.entity.PurchaseRequest;
import com.bootcamp.infraestructure.document.ValidTransactions;
import com.bootcamp.infraestructure.service.ValidTransactionService;
import java.util.Date;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/** events service for purchase request. */
@Component
@Slf4j
public class PurchaseRequestEventsService {
  @Autowired private ValidTransactionService validTransactionService;

  @Autowired private ValidTransactionEventsService validTransactionEventsService;

  @Autowired private KafkaTemplate<String, Event<?>> producer;

  @Value("${topic.purchaseRequestValidated.name:purchaseRequestValidated}")
  private String topicBankAccount;

  public void publish(PurchaseRequest purchaseRequest) {
    PurchaseRequestCreatedEvent createdEvent = new PurchaseRequestCreatedEvent();
    createdEvent.setData(purchaseRequest);
    createdEvent.setId(UUID.randomUUID().toString());
    createdEvent.setType(EventType.CREATED);
    createdEvent.setDate(new Date());

    this.producer.send(topicBankAccount, createdEvent);
  }

  @KafkaListener(
      topics = "${topic.purchaseRequestVenta.name:purchaseRequestVenta}",
      containerFactory = "kafkaListenerContainerFactory",
      groupId = "11")
  public void consumer(Event<?> event) {
    if (event.getClass().isAssignableFrom(PurchaseRequestCreatedEvent.class)) {
      PurchaseRequestCreatedEvent createdEvent = (PurchaseRequestCreatedEvent) event;
      log.info(
          "Received purchaseRequest created event .... with Id={}, data={}",
          event.getId(),
          event.getData());
      // purchaseRequestRepository.save(createdEvent.getData()).subscribe();
      String validTransactionId = UUID.randomUUID().toString();
      Date dateActual = new Date();
      PurchaseRequest purchaseRequestValid = createdEvent.getData();
      purchaseRequestValid.setValidated("valid");
      ValidTransactions validTransactions =
          new ValidTransactions(validTransactionId, dateActual, purchaseRequestValid);
      validTransactionService.save(validTransactions).subscribe();
      this.validTransactionEventsService.publish(purchaseRequestValid);
    }
  }
}
