package com.bootcamp.infraestructure.service;

import com.bootcamp.infraestructure.document.ValidTransactions;
import reactor.core.publisher.Mono;

/** valid transaction service. */
public interface ValidTransactionService {

  Mono<ValidTransactions> save(ValidTransactions validTransactions);
}
