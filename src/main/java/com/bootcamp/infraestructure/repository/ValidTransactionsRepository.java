package com.bootcamp.infraestructure.repository;

import com.bootcamp.infraestructure.document.ValidTransactions;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

/** repository for valid transactions. */
@Repository
public interface ValidTransactionsRepository
    extends ReactiveMongoRepository<ValidTransactions, String> {}
