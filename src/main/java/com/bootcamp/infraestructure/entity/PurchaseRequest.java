package com.bootcamp.infraestructure.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/** purchase request entity. */
@Getter
@Setter
@AllArgsConstructor
public class PurchaseRequest {
  private String id;
  private String requesterDocument;
  private String sellerDocument;
  private String totalAmount;
  private String paymentMethod;
  private String accepted;
  private String validated;
}
