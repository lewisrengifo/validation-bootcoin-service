package com.bootcamp.infraestructure.document;

import com.bootcamp.infraestructure.entity.PurchaseRequest;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/** document valid transactions. */
@Getter
@Setter
@AllArgsConstructor
@Document(collection = "validTransactions")
public class ValidTransactions {
  private String id;
  private Date date;
  private PurchaseRequest purchaseRequest;
}
