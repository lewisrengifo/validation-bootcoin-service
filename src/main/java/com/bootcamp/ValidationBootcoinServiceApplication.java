package com.bootcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/** spring application. */
@SpringBootApplication
public class ValidationBootcoinServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(ValidationBootcoinServiceApplication.class, args);
  }
}
